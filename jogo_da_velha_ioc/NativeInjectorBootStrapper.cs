using Microsoft.Extensions.DependencyInjection;
using jogo_da_velha_crosscutting;


namespace jogo_da_velha_ioc
{
    public static class NativeInjectorBootStrapper
    {
        public static void RegisterAppDependencies(this IServiceCollection service)
        {
            ConfigureServices.ConfigureDependenciesService(service);
            ConfigureMappers.ConfigureDependenciesMapper(service);
        }

        public static void RegisterAppDependenciesContext(this IServiceCollection service)
        {
            ConfigureRepository.ConfigureDependenciesRepository(service);
        }
    }
}