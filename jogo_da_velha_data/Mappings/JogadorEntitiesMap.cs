using jogo_da_velha_domain_entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace jogo_da_velha_data_mappings
{
    public class JogadorEntitiesMap : IEntityTypeConfiguration<JogadorEntitie>
    {
        public void Configure(EntityTypeBuilder<JogadorEntitie> builder)
        {
            builder.HasKey(x => x.Id);
            builder.ToTable("jogadores");
        }
    }

}