﻿using Microsoft.EntityFrameworkCore;
using jogo_da_velha_domain_entities;
using jogo_da_velha_data_mappings;

namespace jogo_da_velha_data_context
{
    public class Context : DbContext
    {
        public DbSet<JogadaEntitie> Jogadas {get; set;}
        public DbSet<JogadorEntitie> Jogadores {get; set;}
        public DbSet<TabuleiroEntitie> Tabuleiros {get; set;}

        // protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        // {
        //     optionsBuilder.UseSqlServer(@"Server=44.199.200.211,3309;Database=universidade_luigi_castro;User=luigi_castro;Password=PcRgWM8LZLi3haE3BBWu");
        // }

        public Context(DbContextOptions<Context> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<JogadaEntitie>(new JogadaEntitiesMap().Configure);
            modelBuilder.Entity<JogadorEntitie>(new JogadorEntitiesMap().Configure);
        }
    }
}


