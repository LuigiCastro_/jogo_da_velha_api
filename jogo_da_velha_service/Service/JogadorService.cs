using jogo_da_velha_domain_interfaces_repository;
using jogo_da_velha_domain_interfaces_service;
using jogo_da_velha_domain_enums;
using jogo_da_velha_domain_entities;
using jogo_da_velha_domain_contracts_jogador;
using AutoMapper;


namespace jogo_da_velha_service
{
    public class JogadorService : IJogadorService
    {
        public readonly IJogadorRepository _jogadorRepository;
        public readonly IMapper _mapper;

        public JogadorService(IJogadorRepository jogadorRepository, IMapper mapper)
        {
            _jogadorRepository = jogadorRepository;
            _mapper = mapper;
        }

        
    
        public async Task<JogadorReponse> AddAsync(JogadorRequest jogadorRequest)
        {
            var requestJogadorEntitie = _mapper.Map<JogadorEntitie>(jogadorRequest);
            var jogadorRegistrado = await _jogadorRepository.AddAsync(requestJogadorEntitie);
            return _mapper.Map<JogadorReponse>(jogadorRegistrado);
        }

        public async Task<JogadorReponse> FindAsync(int id)
        {
            var jogadorEncontrado = await _jogadorRepository.FindAsync(id);
            return _mapper.Map<JogadorReponse>(jogadorEncontrado);
        }
        public async Task <List<JogadorReponse>> ListAsync()
        {
            var jogadoresListados = await _jogadorRepository.ListAsync();
            return _mapper.Map<List<JogadorReponse>>(jogadoresListados);
        }
        public async Task<JogadorReponse> EditAsync(JogadorRequest jogadorRequest, int? id)
        {
            var jogadorEncontrado = await _jogadorRepository.FindAsync((int)id);
            jogadorEncontrado.Nome = jogadorRequest.Nome;
            var jogadorEditado = await _jogadorRepository.EditAsync(jogadorEncontrado, null);
            return _mapper.Map<JogadorReponse>(jogadorEditado);
        }
        public async Task Remove(int id)
        {
            var jogadorEncontrado = await _jogadorRepository.FindAsync((int)id);
            if (jogadorEncontrado != null)
            {
                await _jogadorRepository.Remove(jogadorEncontrado);
            }
        }





    }
}