using jogo_da_velha_domain_interfaces_repository;
using jogo_da_velha_domain_interfaces_service;
using jogo_da_velha_domain_enums;
using jogo_da_velha_domain_entities;
using jogo_da_velha_domain_contracts_jogador;
using jogo_da_velha_domain_contracts_jogada;
using AutoMapper;
using System.Linq;

namespace jogo_da_velha_service
{
    public class JogadaService : JogadaEntitie, IJogadaService
    {
        public readonly IJogadaRepository _jogadaRepository;
        public readonly IMapper _mapper;

        public List<JogadaEntitie> registroDeJogadas = new List<JogadaEntitie>{};
        public static TabuleiroEntitie tabuleiro = new();

        public JogadaService(IJogadaRepository jogadaRepository, IMapper mapper)
        {
            _jogadaRepository = jogadaRepository;
            _mapper = mapper;
        }

        public StatusJogada ExecutarJogada(TipoJogador jogador, int linha, int coluna)
        {
            var jogada = new JogadaEntitie{Jogador = jogador, Linha = linha, Coluna = coluna};

            if(jogador == null)
            {
                throw new Exception("Jogador não pode ser vazio");
            }
            if(linha > 2 || linha < 0 || coluna > 2 || coluna < 0)
            {
                throw new Exception("Numero de linha ou coluna fora do permitido");
            }
            if(linha == null || coluna == null)
            {
                throw new Exception("Linha ou coluna não pode ser vazio");
            }
            if(registroDeJogadas.Contains(jogada))
            {
                throw new Exception(StatusJogada.INVALIDA.ToString());
            }

            _jogadaRepository.Jogar(jogador, linha, coluna);
            _jogadaRepository.RegistrarJogada(jogada);
            return StatusJogada.VALIDA;
        }
        public void IterarJogo(JogadaEntitie jogada)
        {
            for(int lance = 1; lance <= Movimentos; lance++)
            {
                if(registroDeJogadas.Count == 0)
                {
                    ExecutarJogada(jogada.Jogador, jogada.Linha, jogada.Coluna);
                }
                else if(registroDeJogadas.Count > 0) 
                {
                    var ultimoLance = registroDeJogadas[registroDeJogadas.Count()-1];
                    
                    if(ultimoLance.Jogador == TipoJogador.O)
                    {
                        ExecutarJogada(jogada.Jogador, jogada.Linha, jogada.Coluna);
                    }
                    else if(ultimoLance.Jogador == TipoJogador.X)
                    {
                        ExecutarJogada(jogada.Jogador, jogada.Linha, jogada.Coluna);
                    }        
                }
            }
        }


        public StatusJogo DefinirVencedor(TipoJogador jogador)
        {
            for(int i = 0; i <= 2; i++)
            {
                if(tabuleiro.Posiçoes[i,0] == jogador &&
                tabuleiro.Posiçoes[i,1] == jogador &&
                tabuleiro.Posiçoes[i,2] == jogador)
                {return StatusJogo.VENCEU;}
                
                if(tabuleiro.Posiçoes[0,i] == jogador &&
                tabuleiro.Posiçoes[1,i] == jogador &&
                tabuleiro.Posiçoes[2,i] == jogador)
                {return StatusJogo.VENCEU;}

                if(tabuleiro.Posiçoes[0,0] == jogador &&
                tabuleiro.Posiçoes[1,1] == jogador &&
                tabuleiro.Posiçoes[2,2] == jogador)
                {return StatusJogo.VENCEU;}

                if(tabuleiro.Posiçoes[0,2] == jogador &&
                tabuleiro.Posiçoes[1,1] == jogador &&
                tabuleiro.Posiçoes[2,0] == jogador)
                {return StatusJogo.VENCEU;}

                else
                {return StatusJogo.VELHA;}         
            }
            return StatusJogo.FIM;
        }
        
        
        



        public async Task<JogadaReponse> AddAsync(JogadaRequest jogadaRequest)
        {
            var requestJogadaEntitie = _mapper.Map<JogadaEntitie>(jogadaRequest);
            var jogadaRegistrada = await _jogadaRepository.AddAsync(requestJogadaEntitie);
            return _mapper.Map<JogadaReponse>(jogadaRegistrada);
        }

        public async Task<JogadaReponse> FindAsync(int id)
        {
            var jogadaEncontrada = await _jogadaRepository.FindAsync(id);
            return _mapper.Map<JogadaReponse>(jogadaEncontrada);
        }
        public async Task <List<JogadaReponse>> ListAsync()
        {
            var jogadasListadas = await _jogadaRepository.ListAsync();
            return _mapper.Map<List<JogadaReponse>>(jogadasListadas);
        }
        public async Task<JogadaReponse> EditAsync(JogadaRequest jogadaRequest, int? id)
        {
            var jogadaEncontrada = await _jogadaRepository.FindAsync((int)id);
            jogadaEncontrada.Jogador = jogadaRequest.Jogador;
            var jogadaEditada = await _jogadaRepository.EditAsync(jogadaEncontrada, null);
            return _mapper.Map<JogadaReponse>(jogadaEditada);
        }
        public async Task Remove(int id)
        {
            var jogadaEncontrada = await _jogadaRepository.FindAsync((int)id);

            if (jogadaEncontrada != null)
            {
                await _jogadaRepository.Remove(jogadaEncontrada);
            }
        }





    }
}