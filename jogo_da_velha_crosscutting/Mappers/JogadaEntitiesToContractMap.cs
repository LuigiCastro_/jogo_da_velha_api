using jogo_da_velha_domain_contracts_jogada;
using jogo_da_velha_domain_entities;
using AutoMapper;

namespace jogo_da_velha_crosscutting_mappers
{
    public class JogadaEntitiesToContractMap : Profile
    {
        public JogadaEntitiesToContractMap()
        {
            CreateMap<JogadaEntitie, JogadaRequest>().ReverseMap();
            CreateMap<JogadaEntitie, JogadaReponse>().ReverseMap();
        }       
    }
}