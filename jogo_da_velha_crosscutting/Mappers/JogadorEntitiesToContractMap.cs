using jogo_da_velha_domain_contracts_jogador;
using jogo_da_velha_domain_entities;
using AutoMapper;

namespace jogo_da_velha_crosscutting_mappers
{
    public class JogadorEntitiesToContractMap : Profile
    {
        public JogadorEntitiesToContractMap()
        {
            CreateMap<JogadorEntitie, JogadorRequest>().ReverseMap();
            CreateMap<JogadorEntitie, JogadorReponse>().ReverseMap();
        }       
    }
}