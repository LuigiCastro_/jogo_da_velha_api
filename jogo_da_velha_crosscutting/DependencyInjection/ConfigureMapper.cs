using Microsoft.Extensions.DependencyInjection;
using jogo_da_velha_crosscutting_mappers;

namespace jogo_da_velha_crosscutting
{
    public static class ConfigureMappers
    {
        public static void ConfigureDependenciesMapper(IServiceCollection serviceCollection)
        {
            var config = new AutoMapper.MapperConfiguration(cnf => 
            {
                cnf.AddProfile(new JogadaEntitiesToContractMap());
                cnf.AddProfile(new JogadorEntitiesToContractMap());
            });

            var mapConfiguration = config.CreateMapper();
            serviceCollection.AddSingleton(mapConfiguration);
        }
    }
}