using Microsoft.Extensions.DependencyInjection;
using jogo_da_velha_domain_interfaces_service;
using jogo_da_velha_service;
using jogo_da_velha_domain_entities;
using jogo_da_velha_domain_contracts_jogador;
using jogo_da_velha_domain_contracts_jogada;


namespace jogo_da_velha_crosscutting
{
    public static class ConfigureServices
    {
        public static void ConfigureDependenciesService(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IJogadaService, JogadaService>();
            serviceCollection.AddScoped<IJogadorService, JogadorService>();

        }
    }
}