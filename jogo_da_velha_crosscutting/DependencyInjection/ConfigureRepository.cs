using Microsoft.Extensions.DependencyInjection;
using jogo_da_velha_domain_interfaces_repository;
using jogo_da_velha_repository;

namespace jogo_da_velha_crosscutting;

    public static class ConfigureRepository
    {
        public static void ConfigureDependenciesRepository(IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IJogadaRepository, JogadaRepository>();
            serviceCollection.AddScoped<IJogadorRepository, JogadorRepository>();

        }
    }