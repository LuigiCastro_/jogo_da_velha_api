using jogo_da_velha_domain_enums;

namespace jogo_da_velha_domain_entities;

public class JogadorEntitie
{
    // public JogadorEntitie(string nome, string senha, TipoJogador jogador)
    // {
    //     this.Nome = nome;
    //     this.Senha = senha;
    //     this.Jogador = jogador;
    // }
    public int Id {get; set;}
    public string Nome {get; set;}
    public string Senha {get; set;}
    public TipoJogador Jogador {get; set;}
}