using jogo_da_velha_domain_enums;

namespace jogo_da_velha_domain_entities;
public class JogadaEntitie
{
    // public JogadaEntitie(TipoJogador jogador, int linha, int coluna)
    // {
    //     Jogador = jogador;
    //     Linha = linha;
    //     Coluna = coluna;
    // }
    public int Id {get; set; }
    public TipoJogador Jogador {get; set; }
    public StatusJogada Jogada {get; set;}
    public int Linha {get; set; }
    public int Coluna {get; set; }
    public int Movimentos {get; set; } = 9;
}