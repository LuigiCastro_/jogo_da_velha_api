using jogo_da_velha_domain_enums;

namespace jogo_da_velha_domain_entities;

public class TabuleiroEntitie
{
    public int Id {get; set;}
    public TipoJogador[,] Posiçoes = new TipoJogador[3,3];
    // public int linha;
    // public int coluna;
}