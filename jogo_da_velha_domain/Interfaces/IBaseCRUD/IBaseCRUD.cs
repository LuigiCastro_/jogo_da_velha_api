// using jogo_da_velha_data_context;

namespace jogo_da_velha_domain_interfaces
{
    public interface IBaseCRUD<Response, Request>
    {
        Task<Response> AddAsync(Request request);
        Task<Response> FindAsync(int id);
        Task <List<Response>> ListAsync();
        Task<Response> EditAsync(Request request, int? id);
        Task Remove(int id);

    }
}