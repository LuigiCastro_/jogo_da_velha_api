using jogo_da_velha_domain_interfaces;
using jogo_da_velha_domain_entities;
using jogo_da_velha_domain_enums;

namespace jogo_da_velha_domain_interfaces_repository
{
    public interface IJogadaRepository : IBaseCRUD<JogadaEntitie, JogadaEntitie>
    {
        public void Jogar(TipoJogador jogador, int linha, int coluna);
        public void RegistrarJogada(JogadaEntitie jogada);
        Task Remove(JogadaEntitie jogada);
    
    }
}

