using jogo_da_velha_domain_interfaces;
using jogo_da_velha_domain_entities;

namespace jogo_da_velha_domain_interfaces_repository
{
    public interface IJogadorRepository : IBaseCRUD<JogadorEntitie, JogadorEntitie>
    {
        Task Remove(JogadorEntitie jogador);
    }
}