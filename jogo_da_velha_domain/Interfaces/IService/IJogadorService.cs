using jogo_da_velha_domain_interfaces;
using jogo_da_velha_domain_entities;
using jogo_da_velha_domain_contracts_jogador;


namespace jogo_da_velha_domain_interfaces_service
{
    public interface IJogadorService : IBaseCRUD<JogadorReponse, JogadorRequest>
    {
    
    }
}