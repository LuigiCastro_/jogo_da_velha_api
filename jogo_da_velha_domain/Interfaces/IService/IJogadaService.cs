using jogo_da_velha_domain_interfaces;
using jogo_da_velha_domain_entities;
using jogo_da_velha_domain_enums;
using jogo_da_velha_domain_contracts_jogada;

namespace jogo_da_velha_domain_interfaces_service
{
    public interface IJogadaService : IBaseCRUD<JogadaReponse, JogadaRequest>
    {
        public StatusJogada ExecutarJogada(TipoJogador jogador, int linha, int coluna);
        public void IterarJogo(JogadaEntitie jogada);
    
    }
}

