namespace jogo_da_velha_domain_enums;
public enum StatusJogo
{
    VENCEU,
    VELHA,
    FIM,
}