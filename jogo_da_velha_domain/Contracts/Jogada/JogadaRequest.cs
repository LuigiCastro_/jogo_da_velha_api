using jogo_da_velha_domain_enums;

namespace jogo_da_velha_domain_contracts_jogada
{
    public class JogadaRequest
    {
        public TipoJogador Jogador {get; set;}
        public int Linha {get; set; }
        public int Coluna {get; set; }
    }
}