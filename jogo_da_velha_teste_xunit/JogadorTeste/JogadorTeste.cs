using Moq;
using AutoMapper;
using jogo_da_velha_service;
using jogo_da_velha_teste_fakers;
using jogo_da_velha_teste_crosscutting;


namespace jogo_da_velha_teste_xunit;

public class JogadorTeste
{
    private readonly Mock<IJogadorRepository> _mockJogadorRepository = new Mock<IJogadorRepository>();   
    public IMapper mapper = new AutoMapperFixture().GetMapper();


    [Fact(DisplayName = "Registrar um novo jogador")]
    public async Task JogadorPost()
    {
        var jogadorRequest = JogadorContractsFakers.JogadorRequest();
        var jogadorRequestEntities = JogadorEntitiesFaker.JogadorEntitie();
        var resultJogadorRequest = JogadorEntitiesFaker.JogadorEntitiesId(jogadorRequestEntities.Nome);

        _mockJogadorRepository.Setup(mock => mock.AddAsync(It.IsAny<JogadorEntitie>())).Returns(resultJogadorRequest);
        var service = new JogadorService(_mockJogadorRepository.Object, mapper);
        var result = await service.AddAsync(jogadorRequest);

        Assert.Equal(result.Nome, resultJogadorRequest.Result.Nome);
    }

    [Fact(DisplayName = "Buscar um jogador por ID")]
    public async Task JogadorGetById()
    {
        int id = JogadorEntitiesFaker.GetId();

        _mockJogadorRepository.Setup(mock => mock.FindAsync(id)).Returns(JogadorEntitiesFaker.JogadorEntitiesNome(id));
        var service = new JogadorService(_mockJogadorRepository.Object, mapper);
        var result = await service.FindAsync(id);

        Assert.Equal(result.Id, id);
    }

    [Fact(DisplayName = "Listar todos os jogadores")]
    public async Task JogadorGet()
    {
        _mockJogadorRepository.Setup(mock => mock.ListAsync()).Returns(JogadorEntitiesFaker.JogadorEntitiesList());
        var service = new JogadorService(_mockJogadorRepository.Object, mapper);
        var result = await service.ListAsync();

        Assert.True(result.ToList().Count() > 0);
    }

    [Fact(DisplayName = "Editar um jogador já existente")]
    public async Task JogadorPut()
    {
        var jogadorRequest = JogadorContractsFakers.JogadorRequest();
        var jogadorRequestEntities = JogadorEntitiesFaker.JogadorEntitie();
        var resultJogadorRequest = JogadorEntitiesFaker.JogadorEntitiesId(jogadorRequestEntities.Nome);

        _mockJogadorRepository.Setup(mock => mock.FindAsync(It.IsAny<int>())).Returns(resultJogadorRequest);
        _mockJogadorRepository.Setup(mock => mock.EditAsync(It.IsAny<JogadorEntitie>(), It.IsAny<int?>())).Returns(resultJogadorRequest);

        var service = new JogadorService(_mockJogadorRepository.Object, mapper);
        var result = await service.EditAsync(jogadorRequest, resultJogadorRequest.Result.Id);

        Assert.Equal(result.Nome, resultJogadorRequest.Result.Nome);
    }

    [Fact(DisplayName = "Remover um jogador já existente")]
    public async Task JogadorDelete()
    {
        int id = JogadorEntitiesFaker.GetId();
        _mockJogadorRepository.Setup(mock => mock.Remove(id)).Returns(() => Task.FromResult(string.Empty));
        var service = new JogadorService(_mockJogadorRepository.Object, mapper);

        try
        {
            await service.Remove(id);
        }
        catch (System.Exception)
        {
            Assert.True(false);
        }
    }
}