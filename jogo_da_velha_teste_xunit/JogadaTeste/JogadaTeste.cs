using Moq;
using AutoMapper;
using jogo_da_velha_service;
using jogo_da_velha_teste_fakers;
using jogo_da_velha_teste_crosscutting;


namespace jogo_da_velha_teste_xunit;

public class JogadaTeste
{
    private readonly Mock<IJogadaRepository> _mockJogadaRepository = new Mock<IJogadaRepository>();   
    private readonly Mock<IJogadaService> _mockJogadaService = new Mock<IJogadaService>();
    private readonly IJogadaService _jogadaService;
    private IJogadaService _jogada;
    public IMapper mapper = new AutoMapperFixture().GetMapper();


    [Fact(DisplayName = "Registrar uma nova jogada")]
    public async Task JogadaPost()
    {
        var jogadaRequest = JogadaContractsFakers.JogadaRequest();
        var jogadaRequestEntities = JogadaEntitiesFaker.JogadaEntitie();
        var resultJogadaRequest = JogadaEntitiesFaker.JogadaEntitiesId(jogadaRequestEntities.Linha);

        _mockJogadaRepository.Setup(mock => mock.AddAsync(It.IsAny<JogadaEntitie>())).Returns(resultJogadaRequest);
        var service = new JogadaService(_mockJogadaRepository.Object, mapper);
        var result = await service.AddAsync(jogadaRequest);

        Assert.Equal(result.Linha, resultJogadaRequest.Result.Linha);
    }

    [Fact(DisplayName = "Buscar uma jogada por ID")]
    public async Task JogadaGetById()
    {
        int id = JogadaEntitiesFaker.GetId();

        _mockJogadaRepository.Setup(mock => mock.FindAsync(id)).Returns(JogadaEntitiesFaker.JogadaEntitiesLinha(id));
        var service = new JogadaService(_mockJogadaRepository.Object, mapper);
        var result = await service.FindAsync(id);

        Assert.Equal(result.Id, id);
    }

    [Fact(DisplayName = "Listar todos as jogadas")]
    public async Task JogadaGet()
    {
        _mockJogadaRepository.Setup(mock => mock.ListAsync()).Returns(JogadaEntitiesFaker.JogadaEntitiesList());
        var service = new JogadaService(_mockJogadaRepository.Object, mapper);
        var result = await service.ListAsync();

        Assert.True(result.ToList().Count() > 0);
    }

    [Fact(DisplayName = "Editar uma jogada já existente")]
    public async Task JogadaPut()
    {
        var JogadaRequest = JogadaContractsFakers.JogadaRequest();
        var JogadaRequestEntities = JogadaEntitiesFaker.JogadaEntitie();
        var resultJogadaRequest = JogadaEntitiesFaker.JogadaEntitiesId(JogadaRequestEntities.Linha);

        _mockJogadaRepository.Setup(mock => mock.FindAsync(It.IsAny<int>())).Returns(resultJogadaRequest);
        _mockJogadaRepository.Setup(mock => mock.EditAsync(It.IsAny<JogadaEntitie>(), It.IsAny<int?>())).Returns(resultJogadaRequest);

        var service = new JogadaService(_mockJogadaRepository.Object, mapper);
        var result = await service.EditAsync(JogadaRequest, resultJogadaRequest.Result.Id);

        Assert.Equal(result.Linha, resultJogadaRequest.Result.Linha);
    }

    [Fact(DisplayName = "Remover uma jogada já existente")]
    public async Task JogadaDelete()
    {
        int id = JogadaEntitiesFaker.GetId();
        _mockJogadaRepository.Setup(mock => mock.Remove(id)).Returns(() => Task.FromResult(string.Empty));
        var service = new JogadaService(_mockJogadaRepository.Object, mapper);

        try
        {
            await service.Remove(id);
        }
        catch (System.Exception)
        {
            Assert.True(false);
        }
    }

    [Fact]
    public async Task ExecutarJogadaLinhaAcimaDoLimite()
    {
        _mockJogadaRepository.Setup(repository => repository.Jogar(TipoJogador.X, 3, 0));
        _jogada = new JogadaService(_mockJogadaRepository.Object, mapper);
        
        try
        {
            _jogada.ExecutarJogada(TipoJogador.X, 3, 0);
        }
        catch(Exception ex)
        {
            Assert.Equal("Numero de linha ou coluna fora do permitido", ex.Message);
        }
    }

      [Fact]
    public async Task ExecutarJogadaLinhaAbaixoDoLimite()
    {
        _mockJogadaRepository.Setup(repository => repository.Jogar(TipoJogador.X, -1, 0));
        _jogada = new JogadaService(_mockJogadaRepository.Object, mapper);
        
        try
        {
            _jogada.ExecutarJogada(TipoJogador.X, -1, 0);
        }
        catch(Exception ex)
        {
            Assert.Equal("Numero de linha ou coluna fora do permitido", ex.Message);
        }
    }

    [Fact]
    public async Task ExecutarJogadaColunaAcimaDoLimite()
    {
        _mockJogadaRepository.Setup(repository => repository.Jogar(TipoJogador.X, 0, 3));
        _jogada = new JogadaService(_mockJogadaRepository.Object, mapper);
        
        try
        {
            _jogada.ExecutarJogada(TipoJogador.X, 0, 3);
        }
        catch(Exception ex)
        {
            Assert.Equal("Numero de linha ou coluna fora do permitido", ex.Message);
        }
    }

    [Fact]
    public async Task ExecutarJogadaColunaAbaixoDoLimite()
    {
        _mockJogadaRepository.Setup(repository => repository.Jogar(TipoJogador.X, 0, -1));
        _jogada = new JogadaService(_mockJogadaRepository.Object, mapper);
        
        try
        {
            _jogada.ExecutarJogada(TipoJogador.X, 0, -1);
        }
        catch(Exception ex)
        {
            Assert.Equal("Numero de linha ou coluna fora do permitido", ex.Message);
        }
    }

    [Fact]
    public async Task ExecutarJogadaInvalida()
    {
        
        var jogada = new JogadaEntitie{Jogador = TipoJogador.X, Linha = 1, Coluna = 1};

        _mockJogadaRepository.Setup(repository => repository.RegistrarJogada(jogada));
        _jogada = new JogadaService(_mockJogadaRepository.Object, mapper);
        JogadaEntitie statusJogada = new();
        
        try
        {
            _jogada.ExecutarJogada(TipoJogador.X, 1, 1);
        }
        catch(Exception ex)
        {
            Assert.Equal("INVALIDA", ex.Message);
        }    
    }

    [Fact]
    public async Task ExecutarJogadaValida()
    {
        
        var jogada = new JogadaEntitie{Jogador = TipoJogador.X, Linha = 1, Coluna = 1};

        _mockJogadaRepository.Setup(repository => repository.RegistrarJogada(jogada));
        _jogada = new JogadaService(_mockJogadaRepository.Object, mapper);
        _jogada.ExecutarJogada(TipoJogador.O, 2, 2);
        JogadaEntitie statusJogada = new();
        
        Assert.Equal("VALIDA", statusJogada.Jogada.ToString());  
    }
}