using jogo_da_velha_domain_entities;
using Bogus;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace jogo_da_velha_teste_fakers
{
    public static class JogadorEntitiesFaker
    {
        private static readonly Faker Fake = new Faker();

        public static int GetId()
        {
            return Fake.IndexFaker;
        }

        public static async Task<List<JogadorEntitie>> JogadorEntitiesList()
        {
            var minhaLista = new List<JogadorEntitie>();

            for (int i = 0; i < 5; i++)
            {
                minhaLista.Add(new JogadorEntitie()
                {
                    Id = i,
                    Nome = Fake.Name.FirstName()
                });
            }

            return minhaLista;
        }

        public static async Task<JogadorEntitie> JogadorEntitiesNome(int id)
        {
            return new JogadorEntitie()
            {
                Id = id,
                Nome = Fake.Name.FirstName()
            };
        }

        public static JogadorEntitie JogadorEntitie()
        {
            return new JogadorEntitie
            {
                Nome = Fake.Name.FirstName()
            };
        }

        public static async Task<JogadorEntitie> JogadorEntitiesId(string nome)
        {
            return new JogadorEntitie()
            {
                Id = Fake.IndexFaker,
                Nome = nome
            };
        }
    }
}
