using jogo_da_velha_domain_contracts_jogador;
using System.Collections.Generic;
using System.Threading.Tasks;
using Bogus;

namespace jogo_da_velha_teste_fakers
{
    public static class JogadaContractsFakers
    {
        private static readonly Faker Fake = new Faker();

        public static int GetId()
        {
            return Fake.IndexFaker;
        }

        public static async Task<List<JogadaReponse>> JogadaResponseList()
        {
            var minhaLista = new List<JogadaReponse>();

            for (int i = 0; i < 5; i++)
            {
                minhaLista.Add(new JogadaReponse()
                {
                    Id = i,
                    Linha = Fake.PickRandom(0, 2)
                });
            }

            return minhaLista;
        }

        public static async Task<JogadaReponse> JogadaReponseLinha(int id)
        {
            return new JogadaReponse()
            {
                Id = id,
                Linha = Fake.PickRandom(0, 2)
            };
        }

        public static JogadaRequest JogadaRequest()
        {
            return new JogadaRequest
            {
                Linha = Fake.PickRandom(0, 2)
            };
        }

        public static async Task<JogadaReponse> JogadaResponseId(int linha)
        {
            return new JogadaReponse()
            {
                Id = Fake.IndexFaker,
                Linha = linha
            };
        }
    }
}
