using jogo_da_velha_domain_entities;
using Bogus;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace jogo_da_velha_teste_fakers
{
    public static class JogadaEntitiesFaker
    {
        private static readonly Faker Fake = new Faker();

        public static int GetId()
        {
            return Fake.IndexFaker;
        }

        public static async Task<List<JogadaEntitie>> JogadaEntitiesList()
        {
            var minhaLista = new List<JogadaEntitie>();

            for (int i = 0; i < 5; i++)
            {
                minhaLista.Add(new JogadaEntitie()
                {
                    Id = i,
                    Linha = Fake.PickRandom(0, 2)
                });
            }

            return minhaLista;
        }

        public static async Task<JogadaEntitie> JogadaEntitiesLinha(int id)
        {
            return new JogadaEntitie()
            {
                Id = id,
                Linha = Fake.PickRandom(0, 2)
            };
        }

        public static JogadaEntitie JogadaEntitie()
        {
            return new JogadaEntitie
            {
                Linha = Fake.PickRandom(0, 2)
            };
        }

        public static async Task<JogadaEntitie> JogadaEntitiesId(int linha)
        {
            return new JogadaEntitie()
            {
                Id = Fake.IndexFaker,
                Linha = linha,
            };
        }
    }
}
