using jogo_da_velha_domain_contracts_jogador;
using System.Collections.Generic;
using System.Threading.Tasks;
using Bogus;

namespace jogo_da_velha_teste_fakers
{
    public static class JogadorContractsFakers
    {
        private static readonly Faker Fake = new Faker();

        public static int GetId()
        {
            return Fake.IndexFaker;
        }

        public static async Task<List<JogadorReponse>> JogadorResponseList()
        {
            var minhaLista = new List<JogadorReponse>();

            for (int i = 0; i < 5; i++)
            {
                minhaLista.Add(new JogadorReponse()
                {
                    Id = i,
                    Nome = Fake.Name.FirstName()
                });
            }

            return minhaLista;
        }

        public static async Task<JogadorReponse> JogadorReponseNome(int id)
        {
            return new JogadorReponse()
            {
                Id = id,
                Nome = Fake.Name.FirstName()
            };
        }

        public static JogadorRequest JogadorRequest()
        {
            return new JogadorRequest
            {
                Nome = Fake.Name.FirstName()
            };
        }

        public static async Task<JogadorReponse> JogadorResponseId(string nome)
        {
            return new JogadorReponse()
            {
                Id = Fake.IndexFaker,
                Nome = nome
            };
        }
    }
}
