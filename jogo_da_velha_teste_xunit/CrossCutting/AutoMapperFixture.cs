using jogo_da_velha_crosscutting_mappers;
using AutoMapper;
using System;

namespace jogo_da_velha_teste_crosscutting
{
    public abstract class BaseAutoMapperFixture
    {
        public IMapper mapper { get; set; }

        public BaseAutoMapperFixture()
        {
            mapper = new AutoMapperFixture().GetMapper();
        }
    }

    public class AutoMapperFixture : IDisposable
    {
        public IMapper GetMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new JogadaEntitiesToContractMap());
                cfg.AddProfile(new JogadorEntitiesToContractMap());
            });

            return config.CreateMapper();
        }

        public void Dispose() { }
    }
}
