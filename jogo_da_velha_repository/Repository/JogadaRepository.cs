using jogo_da_velha_domain_enums;
using jogo_da_velha_data_context;
using jogo_da_velha_domain_entities;
using jogo_da_velha_domain_interfaces_repository;
using Microsoft.EntityFrameworkCore;

namespace jogo_da_velha_repository
{
    public class JogadaRepository : IJogadaRepository
    {
        public List<JogadaEntitie> registroDeJogadas = new List<JogadaEntitie>{};
        public static TabuleiroEntitie tabuleiro = new();


        private readonly Context _context;
        public JogadaRepository(Context context)
        {
            _context = context;
        }
        

        public void Jogar(TipoJogador jogador, int linha, int coluna)
            => tabuleiro.Posiçoes[linha, coluna] = jogador;
        
        public void RegistrarJogada(JogadaEntitie jogada)
            => registroDeJogadas.Add(jogada);



        public async Task<JogadaEntitie> AddAsync(JogadaEntitie jogada)
        {
            await _context.Jogadas.AddAsync(jogada);
            await _context.SaveChangesAsync();
            return jogada;
        }
            
        public async Task<JogadaEntitie> FindAsync(int id)
        {
            return await _context.Jogadas.Where(prop => prop.Id == id).AsNoTracking().FirstOrDefaultAsync();
        }

        public async Task <List<JogadaEntitie>> ListAsync()
        {
            return await _context.Jogadas.AsNoTracking().ToListAsync();
        }

        public async Task<JogadaEntitie> EditAsync(JogadaEntitie jogada, int? id = null)
        {
            _context.Jogadas.Update(jogada);
            await _context.SaveChangesAsync();
            return jogada;
        }

        public async Task Remove(JogadaEntitie jogada)
        {
            _context.Jogadas.Remove(jogada);
            await _context.SaveChangesAsync();
        }

        public Task Remove(int jogada)
        {
            throw new NotImplementedException();
        }
    }
}