using jogo_da_velha_domain_enums;
using jogo_da_velha_data_context;
using jogo_da_velha_domain_entities;
using jogo_da_velha_domain_interfaces_repository;
using Microsoft.EntityFrameworkCore;

namespace jogo_da_velha_repository
{
    public class JogadorRepository : IJogadorRepository
    {
        private readonly Context _context;
        public JogadorRepository(Context context)
        {
            _context = context;
        }


        public async Task<JogadorEntitie> AddAsync(JogadorEntitie jogador)
        {
            await _context.Jogadores.AddAsync(jogador);
            await _context.SaveChangesAsync();
            return jogador;
        }
            
        public async Task<JogadorEntitie> FindAsync(int id)
        {
            return await _context.Jogadores.Where(prop => prop.Id == id).AsNoTracking().FirstOrDefaultAsync();
        }
        public async Task <List<JogadorEntitie>> ListAsync()
        {
            return await _context.Jogadores.AsNoTracking().ToListAsync();
        }
        public async Task<JogadorEntitie> EditAsync(JogadorEntitie jogador, int? id = null)
        {
            _context.Jogadores.Update(jogador);
            await _context.SaveChangesAsync();
            return jogador;
        }
        public async Task Remove(JogadorEntitie jogador)
        {
            _context.Jogadores.Remove(jogador);
            await _context.SaveChangesAsync();
        }
        public Task Remove(int jogador)
        {
            throw new NotImplementedException();
        }




        


        // JOGAR
        // REGISTRAR
        // EDITAR
        // BUSCAR
        // LISTAR
        // REMOVER
        

        
    }
}