


# CRIAR UM JOGO DA VELHA UTILIZANDO-SE DA ESTRUTURA SOLID, CLEAN CODE, IOC, DDD, TDD, CRUD
- Utilizar a arquitetura de Web Api
- Popular dados nos testes
- Conectar-se a um banco de dados (ideal: por meio da stringconnection // alternativa: dbcontex)
- Fazer migrations
- Criar classlibs

# To create the following relationship between PROJECTS, FOLDERS n' FILES:
> CONTROLLER
    _WeatherForecastController

> DOMAIN
    > ENTITIES
        _Base
        _Entity
    > INTERFACES
        > IREPOSITORY
            _IBase<T>
            _IRepository
        > ISERVICE
            _IBase<T>
            _IService
    > CONTRACTS
        _Request
        _Response

> SERVICE
    _BaseService
    _EntityService

> REPOSITORY
    _BaseRepository
    _EntityRepository

> CROSSCUTTING
    > AUTOMAPPER
    > DEPENDENCYINJECTION
        _ConfigureService
> IOC
    _Native...
> DATA
    > CONTEXT
        _Context





CONTEXT no repositorio???
ADD projetos à solução???
CONTROLLER???
Comandos HTTP???




# JOGO DA VELHA : oq é necessário?

> Dois jogadores : jogador X, jogador O

> Matriz 3x3 (9 quadrantes)

> VITORIA / EMPATE

> Vitoria : sequencia de 3 quadrantes com mesmo simbolo
    > Sequencia : horizontal / vertical / diagonal      :       
        # Horizontal (3 possib), Vertical (3 possib), Diagonal (2 possib)

> Empate : quando não houver sequencia de 3 quadrantes
        # Deu Velha!


# Comportamento

> 9 casas vazias

> Total de jogadas possíveis: 9     ->      [0,0] [0,1] [0,2]   |   [1,0] [1,1] [1,2]   |   [2,0] [2,1] [2,2]

> Quando escolher uma coordenada (x,y), verificar se está VAZIA ou PREENCHIDA
    # Se VAZIA : JOGADA VALIDA!
    # Se PREENCHIDA : JOGADA INVALIDA!

> Se houver uma sequencia de 3 quadrantes com o mesmo simbolo :
    # VITORIA

> Se não houver sequencia de 3 quadrantes com o mesmo simbolo :
    # EMPATE (VELHA)

> Jogo começa com X, depois O       ->      J1:X, J2:O, J3:X, J4:O, J5:X, J6:O, J7:X, J8:O, J9:X


# POSSÍVEIS ENTIDADES
> JOGADOR X
> JOGADOR O
> JOGADA
> TABULEIRO
> POSIÇÃO
> MOVIMENTO


tabuleiro       ->  possui blocos, array[3,3] de tamanho 9
jogadorX        ->  
jogadorO        ->
baseJogador     ->  id, nome
jogadas         ->  id, posições [x,y]

enumStatus      -> valida, invalida, venceu, velha              # Domain (aqui entidades)

repositório     -> jogada, array[3,3] de tamanho 9
                -> metodo jogar(jogador, linha, coluna)

service         -> metodo jogar(jogador, linha, coluna)
                -> metodo registrar jogada
                -> metodo exibir jogada
                -> validar jogador
                -> validar bloco vazio / não

interfaces      -> IRepository
                -> IService
                -> IBaseCRUD



