using Microsoft.AspNetCore.Mvc;
using jogo_da_velha_domain_entities;
using jogo_da_velha_domain_enums;
using jogo_da_velha_domain_interfaces_service;
using jogo_da_velha_service;
using jogo_da_velha_domain_contracts_jogada;
using jogo_da_velha_domain_contracts_jogador;


namespace jogo_da_velha_api.Controllers;

[ApiController]
[Route("[controller]")]
public class JogadaController : ControllerBase
{

    private readonly IJogadaService _jogadaService;

    public JogadaController(IJogadaService jogadaService)
    {
        _jogadaService = jogadaService;
    }
    
    [HttpPost]
    public async Task<JogadaReponse> Post(JogadaRequest jogadaRequest)
    {
        return await _jogadaService.AddAsync(jogadaRequest);
    }

    [HttpGet("{id}")]
    public async Task<JogadaReponse> GetById(int id)
    {
        return await _jogadaService.FindAsync(id);
    }

    [HttpGet]
    public async Task<IEnumerable<JogadaReponse>> Get()
    {
        return await _jogadaService.ListAsync();
    }

    [HttpPut("{id}")]
    public async Task<JogadaReponse> Put(int id, JogadaRequest jogadaRequest)
    {
        return await _jogadaService.EditAsync(jogadaRequest, id);
    }

    [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await _jogadaService.Remove(id);
        }
}
