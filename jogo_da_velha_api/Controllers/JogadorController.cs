using Microsoft.AspNetCore.Mvc;
using jogo_da_velha_domain_entities;
using jogo_da_velha_domain_enums;
using jogo_da_velha_domain_interfaces_service;
using jogo_da_velha_service;
using jogo_da_velha_domain_contracts_jogada;
using jogo_da_velha_domain_contracts_jogador;


namespace jogo_da_velha_api.Controllers;

[ApiController]
[Route("[controller]")]
public class JogadorController : ControllerBase
{

    private readonly IJogadorService _jogadorService;

    public JogadorController(IJogadorService jogadorService)
    {
        _jogadorService = jogadorService;
    }
    
    [HttpPost]
    public async Task<JogadorReponse> Post(JogadorRequest jogadorRequest)
    {
        return await _jogadorService.AddAsync(jogadorRequest);
    }

    [HttpGet("{id}")]
    public async Task<JogadorReponse> GetById(int id)
    {
        return await _jogadorService.FindAsync(id);
    }

    [HttpGet]
    public async Task<IEnumerable<JogadorReponse>> Get()
    {
        return await _jogadorService.ListAsync();
    }

    [HttpPut("{id}")]
    public async Task<JogadorReponse> Put(int id, JogadorRequest jogadorRequest)
    {
        return await _jogadorService.EditAsync(jogadorRequest, id);
    }

    [HttpDelete("{id}")]
        public async Task Delete(int id)
        {
            await _jogadorService.Remove(id);
        }
}
